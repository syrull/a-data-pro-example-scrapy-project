# Scrapy Crawler Example

Проекта представлява **crawler** който взима всички резултати от първата страница на ardes. След като се вземат тези данни те биват преобразувани в `ArdesItem`-и които след това влизат в `AdataproCrawlerPipeline` където се запазват в sqlite база данни.

Това е примерен проект който е част от урока за [Web Scrapping](https://colab.research.google.com/drive/1cv9k4IUF5AE3CL5eM_VXOxYlW3uvvvV8?usp=sharing)

# Инсталация

Първо трябва да имате инсталирани следните неща:

* poetry
* python3.8+

[Инсталация на Poetry](https://python-poetry.org/docs/) - Важно е **poetry** да бъде инсталиран със същите права както и **Python**.

## Инсталиране

```console
$ git clone git@gitlab.com:pwnm3rcury/a-data-pro-example-scrapy-project.git && cd ./a-data-pro-example-scrapy-project.git
```
```console
$ poetry install 
```
```console
$ poetry shell
```
```console
(a-data-pro-example-scrapy-project) $ scrapy crawl ardes
```

