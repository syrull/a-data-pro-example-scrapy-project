import scrapy
from adatapro_crawler.items import ArdesItem


class ArdesSpider(scrapy.Spider):
    name = "ardes"
    allowed_domains = ["https://ardes.bg/"]
    start_urls = ["https://ardes.bg/laptopi/laptopi/page/1"]

    def parse(self, response):
        item = ArdesItem()
        for l in response.xpath("""//*[@class="product"]"""):
            item = {
                "title": l.xpath(""".//div[@class="title"]//span/text()""").get(),
                "rating": float(
                    l.xpath(""".//div[@class="rating col-md-9"]//span/text()""")
                    .get()
                    .replace("(", "")
                    .replace(")", "")
                ),
                "parameters": [i.get() for i in l.xpath(""".//ul/li/text()""")],
                "price": float(
                    l.xpath(""".//span[@class="price-num"]/text()""").get()
                    + "."
                    + l.xpath(""".//span[@class="price-num"]//sup/text()""").get()
                ),
            }
            yield item
